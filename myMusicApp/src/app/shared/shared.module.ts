import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SidebarComponent } from './organisms/sidebar/sidebar.component';
import { CardComponent } from './organisms/card/card.component';
import { LogoComponent } from './atoms/logo/logo.component';
import { ButtonComponent } from './atoms/button/button.component';
import { SpotifyService } from '../services/spotify.service';




@NgModule({
  declarations: [
    SidebarComponent,
    CardComponent,
    LogoComponent,
    ButtonComponent
  ],
  imports: [
    CommonModule
  ],
  exports:  [
    ButtonComponent,LogoComponent,SidebarComponent,CardComponent
    
  ],providers:
  [SpotifyService]
})
export class SharedModule { }
