import { WrappedNodeExpr } from '@angular/compiler';
import { Component, OnInit, Output } from '@angular/core';
import { SpotifyService } from '../../services/spotify.service';
import { Album } from '../../services/spotify.interface';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
  
})
export class HomeComponent {

  nuevasCanciones: any[] = [];
  
  

  constructor(private spotifyService: SpotifyService){
   this.spotifyService.getNewReleases()
   .subscribe( (data:any) => {
    
    console.log(data.albums.items);
    this.nuevasCanciones = data.albums.items;
    
    // console.log("ACA SE ACCEDE A ALBUMS ");
    
    // console.log(data.albums);
    // // console.log("ACA SE ACCEDE A ITEMS ");
    
    // // console.log(data.item);

    

    
  });
  }

}
